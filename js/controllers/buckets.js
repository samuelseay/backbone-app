define('controllers/buckets', ['backbone', 'views/bucketslist'], function(backbone, bucketsListView) {
  var buckets;
  buckets = function() {
    var pageRouter;
    pageRouter = Backbone.Router.extend({
      routes: {
        "buckets": "buckets"
      },
      buckets: function() {
        var view;
        view = new bucketsListView();
      }
    });
    new pageRouter();
  };
  return new buckets();
});
