define('controllers/notfound',['backbone'],->
	notFound = ->
			#TODO create a base controller that constructs up page router
			#taking arguments defining routes perhaps? or have base controller
			#setRoutes function
			pageRouter = Backbone.Router.extend({
				routes:
					"*notFound":		"notfound" 
				notfound: ->
					alert "Route not found!"
					return
			})

			new pageRouter()

			return

	new notFound()
)