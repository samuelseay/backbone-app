define('controllers/controllers', ['controllers/notfound', 'controllers/home', 'controllers/buckets'], function(notFound, home, buckets) {
  var controllers;
  controllers = {
    homeController: home,
    bucketsController: buckets,
    notFoundController: notFound
  };
  return Backbone.history.start();
});
