define('controllers/controllers', ['controllers/home', 'controllers/buckets'], function(home, buckets) {
  var controllers;
  controllers = {
    homeController: home,
    bucketsController: buckets
  };
  return Backbone.history.start();
});
