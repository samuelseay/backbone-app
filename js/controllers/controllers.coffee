#defining collection of all controllers here allows us to set up routes all in one
#404 controller (not found) is declared first to set up 
define( 'controllers/controllers',
	    ['controllers/notfound','controllers/home','controllers/buckets'], 
		(notFound,home,buckets) ->
			controllers =
				homeController: home,
				bucketsController: buckets,
				notFoundController: notFound

			Backbone.history.start() 
)