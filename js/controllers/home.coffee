define( 'controllers/home', ['backbone'],
	->
		home = ->
			pageRouter = Backbone.Router.extend({
				routes:
					"home":		"home" 
				home: ->
					alert "home route"
					return
			})

			new pageRouter()

			return

		new home()
)