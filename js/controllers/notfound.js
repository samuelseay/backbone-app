define('controllers/notfound', ['backbone'], function() {
  var notFound;
  notFound = function() {
    var pageRouter;
    pageRouter = Backbone.Router.extend({
      routes: {
        "*path": "notfound"
      },
      notfound: function() {
        alert("Route not found!");
      }
    });
    new pageRouter();
  };
  return new notFound();
});
