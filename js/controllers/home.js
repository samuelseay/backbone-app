define('controllers/home', ['backbone'], function() {
  var home;
  home = function() {
    var pageRouter;
    pageRouter = Backbone.Router.extend({
      routes: {
      	"": "home",
        "home": "home"
      },
      home: function() {
        alert("home route");
      }
    });
    new pageRouter();
  };
  return new home();
});
