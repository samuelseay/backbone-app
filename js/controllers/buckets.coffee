define( 'controllers/buckets',['backbone','views/bucketslist'], 
(backbone,bucketsListView)->
	buckets = ->
			#TODO create a base controller that constructs up page router
			#taking arguments defining routes perhaps? or have base controller
			#setRoutes function
			pageRouter = Backbone.Router.extend({
				routes:
					"buckets":		"buckets" 
				buckets: ->
					view = new bucketsListView()
					return
			})

			new pageRouter()

			return

	new buckets()
)