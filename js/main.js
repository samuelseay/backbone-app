(function() {
  var boot;
  require.config({
    baseUrl: 'js',
    paths: {
      app: 'app',
      models: 'models',
      collections: 'collections',
      views: 'views',
      controllers: 'controllers',
      jquery: 'lib/jquery-2.0.3.min',
      backbone: 'lib/backbone-min',
      underscore: 'lib/underscore-min',
      backboneLocalStorage: 'lib/backbone.localStorage-min',
      text: 'lib/text'
    },
    shim: {
      backbone: {
        deps: ['underscore', 'jquery'],
        exports: 'Backbone'
      },
      underscore: {
        exports: '_'
      },
      backboneLocalStorage: {
        deps: ['backbone'],
        exports: 'Store'
      }
    }
  });
  boot = function() {
    require(['controllers/controllers'], function() {});
  };
  boot();
})();
