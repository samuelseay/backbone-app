define('collections/buckets', ['models/bucket', 'backbone', 'backboneLocalStorage'], function(bucketModel) {
  var bucketCollection;
  bucketCollection = Backbone.Collection.extend({
    model: bucketModel,
    localStorage: new Store('budget-buckets'),
    comparator: function(bucket) {
      return bucket.get("title").toLowerCase();
    }
  });
  return new bucketCollection();
});
