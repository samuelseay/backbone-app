define('collections/buckets',['models/bucket','backbone','backboneLocalStorage'],
	(bucketModel)->
		bucketCollection = Backbone.Collection.extend(
			model: bucketModel
			localStorage: new Store('budget-buckets')
			comparator: (bucket)->
				return bucket.get("title").toLowerCase()
		)

		return new bucketCollection()
)