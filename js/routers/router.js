define('routers/router', ['backbone'], function() {
  var pageRouter;
  pageRouter = Backbone.Router.extend({
    routes: {
      "home": "home"
    },
    home: function() {},
    run: function() {
      Backbone.history.start({
        pushState: true
      });
      alert("it has begun");
    }
  });
  return new pageRouter();
});
