define('views/bucketslist', ['collections/buckets', 'models/bucket', 'views/bucket', 'text!templates/bucketslist.tmpl'], function(buckets, bucket, bucketView, listTmpl) {
  var bucketsList;
  return bucketsList = Backbone.View.extend({
    initialize: function() {
      this.listenTo(this.collection, "add", function() {
        return this.collection.sort();
      });
      this.listenTo(this.collection, "sync", this.render);
      this.listenTo(this.collection, "change:title", function(model) {
        return model.collection.sort();
      });
      buckets.fetch();
    },
    el: "#buckets",
    template: _.template(listTmpl),
    collection: buckets,
    events: {
      'click #add-bucket': 'createBucket'
    },
    generateBucket: function() {
      return {
        title: this.titleInput.val(),
        category: this.categoryInput.val()
      };
    },
    render: function() {
      $(this.$el).html(this.template());
      this.titleInput = this.$("#bucket-title");
      this.categoryInput = this.$("#bucket-category");
      this.addBuckets();
      return this;
    },
    addBucket: function(model) {
      var view;
      view = new bucketView({
        model: model
      });
      $("#bucket-list").append(view.render().el);
    },
    addBuckets: function() {
      return this.collection.each(this.addBucket, this);
    },
    createBucket: function() {
      return buckets.create(this.generateBucket());
    }
  });
});
