define('views/bucket',['text!templates/bucket.tmpl','backbone','backboneLocalStorage'],
	(itemTmpl)->
		bucket = Backbone.View.extend(
			tagName:  'li'
			template: _.template(itemTmpl)
			events:
				'click .edit-bucket': 'editBucket'
			render: ->
				this.$el.html( this.template( this.model.toJSON() ) )
				this.title = this.$(".bucket-title")
				this.category = this.$(".bucket-category")
				this.editButton = this.$(".edit-bucket")
				return this
			editBucket: (model) ->
				if  this.editButton.text() == "Edit" 
					this.title.removeAttr("readonly")
					this.category.removeAttr("readonly")
					this.editButton.text("Save")
					this.model.sync()
				else
					this.title.attr("readonly","readonly")
					this.category.attr("readonly","readonly")
					this.editButton.text("Edit")
				return
		)
)