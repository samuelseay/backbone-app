define('views/bucket', ['text!templates/bucket.tmpl', 'backbone'], function(itemTmpl) {
  var bucket;
  return bucket = Backbone.View.extend({
    tagName: 'li',
    template: _.template(itemTmpl),
    events: {
      'click .edit-bucket': 'editBucket'
    },
    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
      this.title = this.$(".bucket-title");
      this.category = this.$(".bucket-category");
      this.editButton = this.$(".edit-bucket");
      return this;
    },
    editBucket: function(model) {
      if (this.editButton.text() === "Edit") {
        this.title.removeAttr("readonly");
        this.category.removeAttr("readonly");
        this.editButton.text("Save");
      } else {
        this.model.save({ title: this.title.val(), category: this.category.val() });
        this.title.attr("readonly", "readonly");
        this.category.attr("readonly", "readonly");
        this.editButton.text("Edit");
      }
    }
  });
});
