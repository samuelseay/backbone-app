define('views/bucketslist',['collections/buckets','models/bucket','views/bucket','text!templates/bucketslist.tmpl'],
	(buckets,bucket,bucketView,listTmpl)->
		bucketsList = Backbone.View.extend(
			
			initialize: ->			
				this.listenTo(this.collection,"add",->
				 this.collection.sort()
				)
				this.listenTo(this.collection,"sync",this.render)
				this.listenTo(this.collection,"change:title",(model)->
					model.collection.sort()
				)
				buckets.fetch()
				return
			el: "#buckets"
			template: _.template(listTmpl)
			collection: buckets
			events:
				'click #add-bucket': 'createBucket'
			generateBucket:	->
				title: this.titleInput.val()
				category: this.categoryInput.val()
			render: ->
				$(this.$el).html(this.template())
				this.titleInput = this.$("#bucket-title")
				this.categoryInput = this.$("#bucket-category")
				this.addBuckets()
				return this
			addBucket: (model) ->
				view = new bucketView( model: model )
				$("#bucket-list").append( view.render().el )
				return
			addBuckets: ->
				this.collection.each(this.addBucket,this)
			createBucket: ->
				buckets.create(this.generateBucket())
		)
)